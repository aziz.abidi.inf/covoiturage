package com.covoiturage.covoiturage.dto;

import com.covoiturage.covoiturage.model.Route;
import com.covoiturage.covoiturage.model.User;
import jakarta.persistence.*;
import lombok.Data;

@Data
public class CreateReservationDTO {
    private Long routeId;
}
