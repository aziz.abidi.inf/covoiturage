package com.covoiturage.covoiturage.dto;

import com.covoiturage.covoiturage.model.User;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import lombok.Data;

import java.util.Date;

@Data
public class CreateRouteDTO {

    private String departure;
    private String destination;
    private Date departureTime;
    private Long carId;
}
