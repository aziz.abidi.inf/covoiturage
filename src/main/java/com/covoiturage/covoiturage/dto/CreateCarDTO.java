package com.covoiturage.covoiturage.dto;

import lombok.Data;

@Data
public class CreateCarDTO {
    private String brand;
    private String model;
    private String color;
    private int year;
    private String category;
    private String engineType;
    private int power;
}
