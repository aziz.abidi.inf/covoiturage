package com.covoiturage.covoiturage.repository;

import com.covoiturage.covoiturage.model.Route;
import com.covoiturage.covoiturage.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface RouteRepository extends JpaRepository<Route, Long> {
    List<Route> findRoutesByDepartureAndDestination(String departure, String destination);
    List<Route> findRoutesByRider(User user);
}
