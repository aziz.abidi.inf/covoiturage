package com.covoiturage.covoiturage.repository;

import com.covoiturage.covoiturage.model.Car;
import com.covoiturage.covoiturage.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CarRepository extends JpaRepository<Car, Long> {
    Optional<Car> findByIdAndOwner(Long carId, User owner);
}
