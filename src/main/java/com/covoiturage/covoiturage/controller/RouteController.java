package com.covoiturage.covoiturage.controller;

import com.covoiturage.covoiturage.dto.CreateRouteDTO;
import com.covoiturage.covoiturage.exception.CarNotFoundException;
import com.covoiturage.covoiturage.exception.RouteNotFoundException;
import com.covoiturage.covoiturage.exception.UserNotFoundException;
import com.covoiturage.covoiturage.model.Route;
import com.covoiturage.covoiturage.service.RouteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/api/routes")
public class RouteController {

    private final RouteService routeService;

    @Autowired
    public RouteController(RouteService routeService) {
        this.routeService = routeService;
    }



    @GetMapping
    public ResponseEntity<List<Route>> searchRoutes(@RequestParam("departure") String departure,
                                                    @RequestParam("destination") String destination) {
        List<Route> matchingRoutes = routeService.searchRoutes(departure, destination);
        return ResponseEntity.ok(matchingRoutes);
    }

    @GetMapping
    public ResponseEntity<List<Route>> getAll() {
        List<Route> matchingRoutes = routeService.getall();
        return ResponseEntity.ok(matchingRoutes);
    }

    @PostMapping
    public ResponseEntity<Route> proposeRoute(@RequestBody CreateRouteDTO createRouteDTO) throws CarNotFoundException {
        Route proposedRoute = routeService.proposeRoute(createRouteDTO);
        return ResponseEntity.status(HttpStatus.CREATED).body(proposedRoute);
    }

    @GetMapping("/{userId}")
    public ResponseEntity<List<Route>> getRoutesByUser(@PathVariable Long userId) throws UserNotFoundException {
        List<Route> userRoutes = routeService.getRoutesByUser(userId);
        return ResponseEntity.ok(userRoutes);
    }

    @PutMapping("/{routeId}")
    public ResponseEntity<Route> updateRoute(@PathVariable Long routeId,
                                             @RequestBody Route updatedRoute) {
        try {
            Route modifiedRoute = routeService.updateRoute(routeId, updatedRoute);
            return ResponseEntity.ok(modifiedRoute);
        } catch (UserNotFoundException | RouteNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }

    @DeleteMapping("/{routeId}")
    public ResponseEntity<Void> deleteRoute(@PathVariable Long routeId) {
        try {
            routeService.deleteRoute(routeId);
            return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
        } catch (RouteNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }
}
