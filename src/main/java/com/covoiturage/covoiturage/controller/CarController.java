package com.covoiturage.covoiturage.controller;

import com.covoiturage.covoiturage.dto.CreateCarDTO;
import com.covoiturage.covoiturage.exception.CarNotFoundException;
import com.covoiturage.covoiturage.exception.OwnerNotFoundException;
import com.covoiturage.covoiturage.model.Car;
import com.covoiturage.covoiturage.service.CarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/cars")
public class CarController {

    private final CarService carService;

    @Autowired
    public CarController(CarService carService) {
        this.carService = carService;
    }

    @PostMapping
    public ResponseEntity<Car> addCar(@RequestBody CreateCarDTO car) {
        Car addedCar = carService.addCar(car);
        return ResponseEntity.status(HttpStatus.CREATED).body(addedCar);
    }


    @GetMapping
    public ResponseEntity<Car> getMyCars(@RequestBody CreateCarDTO car) {
        Car addedCar = carService.addCar(car);
        return ResponseEntity.status(HttpStatus.CREATED).body(addedCar);
    }

    @DeleteMapping("/{carId}")
    public ResponseEntity<Void> deleteCar(@PathVariable Long ownerId, @PathVariable Long carId) {
        try {
            carService.deleteCar(carId);
            return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
        } catch (CarNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }

    @PutMapping("/{carId}")
    public ResponseEntity<Car> updateCar(@PathVariable Long carId, @RequestBody Car updatedCar) {
        try {
            Car modifiedCar = carService.updateCar(carId, updatedCar);
            return ResponseEntity.ok(modifiedCar);
        } catch (CarNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }
}
