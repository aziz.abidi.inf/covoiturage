package com.covoiturage.covoiturage.controller;

import com.covoiturage.covoiturage.dto.CreateReservationDTO;
import com.covoiturage.covoiturage.exception.ReservationNotFoundException;
import com.covoiturage.covoiturage.exception.RouteNotFoundException;
import com.covoiturage.covoiturage.model.Reservation;
import com.covoiturage.covoiturage.service.ReservationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/reservations")
public class ReservationController {

    private final ReservationService reservationService;

    @Autowired
    public ReservationController(ReservationService reservationService) {
        this.reservationService = reservationService;
    }

    @PostMapping
    public ResponseEntity<Reservation> makeReservation(@RequestBody CreateReservationDTO createReservationDTO)
            throws RouteNotFoundException {
        Reservation createdReservation = reservationService.makeReservation(createReservationDTO);
        return ResponseEntity.status(HttpStatus.CREATED).body(createdReservation);
    }

    @PutMapping("/{reservationId}/approve")
    public ResponseEntity<Void> approveReservation(@PathVariable Long reservationId) {
        try {
            reservationService.approveReservation(reservationId);
            return ResponseEntity.ok().build();
        } catch (ReservationNotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }

    @PutMapping("/{reservationId}/cancel")
    public ResponseEntity<Void> cancelReservation(@PathVariable Long reservationId) {
        try {
            reservationService.cancelReservation(reservationId);
            return ResponseEntity.ok().build();
        } catch (ReservationNotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }
}
