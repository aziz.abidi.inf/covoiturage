package com.covoiturage.covoiturage.controller;

import com.covoiturage.covoiturage.dto.CreateUserDTO;
import com.covoiturage.covoiturage.dto.LoginRequest;
import com.covoiturage.covoiturage.mapper.UserMapper;
import com.covoiturage.covoiturage.model.User;
import com.covoiturage.covoiturage.repository.UserRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/auth")
public class AuthController {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final UserMapper userMapper;

    private final AuthenticationManager authenticationManager;
    private final UserDetailsService userDetailsService;
    public AuthController(UserRepository ourUserRepo, PasswordEncoder passwordEncoder, UserMapper userMapper, AuthenticationManager authenticationManager, UserDetailsService userDetailsService) {
        this.userRepository = ourUserRepo;
        this.passwordEncoder = passwordEncoder;
        this.userMapper = userMapper;
        this.authenticationManager = authenticationManager;
        this.userDetailsService = userDetailsService;
    }

    @PostMapping("/login")
    public ResponseEntity<?> login(@RequestBody LoginRequest loginRequest) {
        try {
            Authentication authentication = authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword())
            );

            return ResponseEntity.ok().build();
        } catch (AuthenticationException e) {
            return ResponseEntity.status(401).build(); // Unauthorized
        }
    }
    @PostMapping("/sign-up")
    public ResponseEntity<Object> saveUser(@RequestBody CreateUserDTO createUserDTO) {
        createUserDTO.setPassword(passwordEncoder.encode(createUserDTO.getPassword()));
        User user = userMapper.CreateUserDTOtoUser(createUserDTO);
        User result = userRepository.save(user);
        if (result.getId() > 0) {
            return ResponseEntity.ok("User Was Saved");
        }
        return ResponseEntity.status(404).body("Error, User Not Saved");
    }
}
