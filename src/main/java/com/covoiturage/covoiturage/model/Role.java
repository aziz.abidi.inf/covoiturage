package com.covoiturage.covoiturage.model;

public enum Role {
    ADMIN,
    PASSENGER,
    DRIVER

}
