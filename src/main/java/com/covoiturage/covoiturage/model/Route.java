package com.covoiturage.covoiturage.model;

import jakarta.persistence.*;
import lombok.Data;

import java.util.Date;

@Data
@Entity
@Table(name = "route")
public class Route {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String departure;
    private String destination;
    private Date departureTime;
    @ManyToOne
    private User rider;
    @ManyToOne
    private Car car;
}
