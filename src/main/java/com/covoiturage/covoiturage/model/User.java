package com.covoiturage.covoiturage.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

import java.util.List;

@Data
@Entity
@Table(name = "user")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String name;
    private String familyName;
    private Date birthday;
    @Column(unique = true)
    private String email;
    @Column(unique = true)
    private String username;
    private String password;
    private String roles;
    private int rating;
    private int phoneNumber;
    private String address;
    @OneToMany(mappedBy = "owner")
    private List<Car> cars;
    @JsonIgnore
    @OneToMany(mappedBy = "rider")
    private List<Route> routes;
}
