package com.covoiturage.covoiturage.service;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class UserInfo {

    private UserDetails getLoggedInUserDetails() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null && authentication.getPrincipal() instanceof UserDetails) {
            return (UserDetails) authentication.getPrincipal();
        }
        throw new RuntimeException();
    }

    public String userName(){
        return getLoggedInUserDetails().getUsername();
    }



    public Collection<? extends GrantedAuthority> authorities(){
        return getLoggedInUserDetails().getAuthorities();
    }
}
