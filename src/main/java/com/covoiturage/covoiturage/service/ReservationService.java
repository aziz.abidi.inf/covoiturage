package com.covoiturage.covoiturage.service;

import com.covoiturage.covoiturage.dto.CreateReservationDTO;
import com.covoiturage.covoiturage.exception.ReservationNotFoundException;
import com.covoiturage.covoiturage.exception.RouteNotFoundException;
import com.covoiturage.covoiturage.mapper.ReservationMapper;
import com.covoiturage.covoiturage.model.Reservation;
import com.covoiturage.covoiturage.model.Route;
import com.covoiturage.covoiturage.model.User;
import com.covoiturage.covoiturage.repository.ReservationRepository;
import com.covoiturage.covoiturage.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ReservationService {

    private final ReservationRepository reservationRepository;

    private final ReservationMapper reservationMapper;

    private final RouteService routeService;

    private final UserInfo userInfo;

    private final UserRepository userRepository;

    @Autowired
    public ReservationService(ReservationRepository reservationRepository, ReservationMapper reservationMapper, RouteService routeService, UserInfo userInfo, UserRepository userRepository) {
        this.reservationRepository = reservationRepository;
        this.reservationMapper = reservationMapper;
        this.routeService = routeService;
        this.userInfo = userInfo;
        this.userRepository = userRepository;
    }

    public Reservation makeReservation(CreateReservationDTO createReservationDTO) throws RouteNotFoundException {
        Route route = routeService.getRoute(createReservationDTO.getRouteId());
        String userName = userInfo.userName();
        User passenger = userRepository.findByUsername(userName).get();
        Reservation reservation = reservationMapper.mapCreateReservationDTOToReservation(passenger, route);
        Reservation save = reservationRepository.save(reservation);
        return save;
    }

    public void approveReservation(Long reservationId) throws ReservationNotFoundException {
        Reservation reservation = reservationRepository.findById(reservationId)
                .orElseThrow(ReservationNotFoundException::new);
        reservation.setApproved(true);
        reservationRepository.save(reservation);
    }

    public void cancelReservation(Long reservationId) throws ReservationNotFoundException {
        Reservation reservation = reservationRepository.findById(reservationId)
                .orElseThrow(ReservationNotFoundException::new);
        reservationRepository.delete(reservation);
    }
}
