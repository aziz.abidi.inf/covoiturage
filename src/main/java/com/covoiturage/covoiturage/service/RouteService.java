package com.covoiturage.covoiturage.service;

import com.covoiturage.covoiturage.dto.CreateRouteDTO;
import com.covoiturage.covoiturage.exception.CarNotFoundException;
import com.covoiturage.covoiturage.exception.RouteNotFoundException;
import com.covoiturage.covoiturage.exception.UserNotFoundException;
import com.covoiturage.covoiturage.mapper.RouteMapper;
import com.covoiturage.covoiturage.model.Car;
import com.covoiturage.covoiturage.model.Route;
import com.covoiturage.covoiturage.model.User;
import com.covoiturage.covoiturage.repository.RouteRepository;
import com.covoiturage.covoiturage.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class RouteService {

    private final RouteRepository routeRepository;
    private final UserRepository userRepository;
    private final RouteMapper routeMapper;
    private final UserInfo userInfo;

    private final CarService carService;

    @Autowired
    public RouteService(RouteRepository routeRepository, UserRepository userRepository, RouteMapper routeMapper, UserInfo userInfo, CarService carService) {
        this.routeRepository = routeRepository;
        this.userRepository = userRepository;
        this.routeMapper = routeMapper;
        this.userInfo = userInfo;
        this.carService = carService;
    }

    public List<Route> getAll() {
        return routeRepository.findAll();
    }

    public List<Route> searchRoutes(String departure, String destination) {
        return routeRepository.findRoutesByDepartureAndDestination(departure, destination);
    }

    public List<Route> getall() {
        return routeRepository.findAll();
    }

    public Route getRoute(Long routeId) throws RouteNotFoundException {
        return routeRepository.findById(routeId).orElseThrow(RouteNotFoundException::new);
    }

    public Route proposeRoute(CreateRouteDTO createRouteDTO) throws CarNotFoundException {
        String username = userInfo.userName();
        User rider = userRepository.findByUsername(username).get();
        Car car = carService.getCar(createRouteDTO.getCarId());
        Route route = routeMapper.mapCreateRouteDTOToRoute(createRouteDTO, rider, car);
        return routeRepository.save(route);
    }

    public List<Route> getRoutesByUser(Long userId) throws UserNotFoundException {
        User user = userRepository.findById(userId)
                .orElseThrow(UserNotFoundException::new);
        return routeRepository.findRoutesByRider(user);
    }

    public Route updateRoute(Long routeId, Route updatedRoute) throws UserNotFoundException, RouteNotFoundException {
        Route route = routeRepository.findById(routeId)
                .orElseThrow(RouteNotFoundException::new);

        route.setDeparture(updatedRoute.getDeparture());
        route.setDestination(updatedRoute.getDestination());
        route.setDepartureTime(updatedRoute.getDepartureTime());

        return routeRepository.save(route);
    }

    public void deleteRoute(Long routeId) throws RouteNotFoundException {
        Route route = routeRepository.findById(routeId)
                .orElseThrow(RouteNotFoundException::new);
        routeRepository.delete(route);
    }

}
