package com.covoiturage.covoiturage.service;

import com.covoiturage.covoiturage.dto.CreateCarDTO;
import com.covoiturage.covoiturage.exception.CarNotFoundException;
import com.covoiturage.covoiturage.mapper.CarMapper;
import com.covoiturage.covoiturage.model.Car;
import com.covoiturage.covoiturage.model.User;
import com.covoiturage.covoiturage.repository.CarRepository;
import com.covoiturage.covoiturage.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CarService {

    private final UserRepository userRepository;
    private final CarRepository carRepository;
    private final UserInfo userInfo;

    private final CarMapper carMapper;

    @Autowired
    public CarService(UserRepository userRepository, CarRepository carRepository, UserInfo userInfo, CarMapper carMapper) {
        this.userRepository = userRepository;
        this.carRepository = carRepository;
        this.userInfo = userInfo;
        this.carMapper = carMapper;
    }

    public Car addCar(CreateCarDTO createCarDTO)  {
        String username = userInfo.userName();
        User owner = userRepository.findByUsername(username).get();
        Car car = carMapper.mapCreateCarDTOToCar(createCarDTO, owner);
        return carRepository.save(car);
    }

    public Car getCar(Long carId) throws CarNotFoundException {
        return carRepository.findById(carId).orElseThrow(CarNotFoundException::new);
    }

    public void deleteCar(Long carId) throws CarNotFoundException {
        Car car = carRepository.findById(carId)
                .orElseThrow(CarNotFoundException::new);
        carRepository.delete(car);
    }

    public Car updateCar(Long carId, Car updatedCar) throws CarNotFoundException {

        Car car = carRepository.findById(carId)
                .orElseThrow(CarNotFoundException::new);

        car.setBrand(updatedCar.getBrand());
        car.setModel(updatedCar.getModel());
        car.setColor(updatedCar.getColor());
        car.setYear(updatedCar.getYear());
        car.setCategory(updatedCar.getCategory());
        car.setEngineType(updatedCar.getEngineType());
        car.setPower(updatedCar.getPower());

        return carRepository.save(car);
    }
}
