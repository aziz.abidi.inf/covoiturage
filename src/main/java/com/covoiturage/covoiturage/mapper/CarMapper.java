package com.covoiturage.covoiturage.mapper;

import com.covoiturage.covoiturage.dto.CreateCarDTO;
import com.covoiturage.covoiturage.model.Car;
import com.covoiturage.covoiturage.model.User;
import org.springframework.stereotype.Service;

@Service
public class CarMapper {
    public Car mapCreateCarDTOToCar(CreateCarDTO createCarDTO, User owner) {
        Car car = new Car();
        car.setBrand(createCarDTO.getBrand());
        car.setModel(createCarDTO.getModel());
        car.setColor(createCarDTO.getColor());
        car.setYear(createCarDTO.getYear());
        car.setCategory(createCarDTO.getCategory());
        car.setEngineType(createCarDTO.getEngineType());
        car.setPower(createCarDTO.getPower());
        car.setOwner(owner);
        return car;
    }
}
