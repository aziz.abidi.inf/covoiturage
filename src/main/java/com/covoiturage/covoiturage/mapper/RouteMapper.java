package com.covoiturage.covoiturage.mapper;

import com.covoiturage.covoiturage.dto.CreateRouteDTO;
import com.covoiturage.covoiturage.model.Car;
import com.covoiturage.covoiturage.model.Route;
import com.covoiturage.covoiturage.model.User;
import org.springframework.stereotype.Service;

@Service
public class RouteMapper {
    public Route mapCreateRouteDTOToRoute(CreateRouteDTO createRouteDTO, User rider, Car car) {
        Route route = new Route();
        route.setDeparture(createRouteDTO.getDeparture());
        route.setDestination(createRouteDTO.getDestination());
        route.setDepartureTime(createRouteDTO.getDepartureTime());
        route.setCar(car);
        route.setRider(rider);
        return route;
    }
}
