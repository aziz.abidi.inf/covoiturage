package com.covoiturage.covoiturage.mapper;

import com.covoiturage.covoiturage.dto.CreateRouteDTO;
import com.covoiturage.covoiturage.model.Car;
import com.covoiturage.covoiturage.model.Reservation;
import com.covoiturage.covoiturage.model.Route;
import com.covoiturage.covoiturage.model.User;
import org.springframework.stereotype.Service;

@Service
public class ReservationMapper {
    public Reservation mapCreateReservationDTOToReservation(User passenger, Route route) {
        Reservation reservation = new Reservation();
        reservation.setPassenger(passenger);
        reservation.setRoute(route);
        return reservation;
    }
}
